#!/bin/bash
#
# Bases variables -----------------------------------------------------------------------------------------------
# ### PATH VARIABLES ### #
LOCAL_PATH="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )" # To know from where we have to source the codes.
SRC_BASE="$LOCAL_PATH/src"
# ### ### #
# ### System Type ### #
OS=$(uname -s)
# ### Local Language ### #
lang=$(locale | grep LANGUAGE | cut -d= -f2 | cut -d_ -f1)
# ### ### #
# ---------------------------------------------------------------------------------------------------------------
# Bases source --------------------------------------------------------------------------------------------------
case "$lang" in 
	fr*) source $SRC_BASE/messages_fr.sh;;
	*) source $SRC_BASE/messages_en.sh;;
esac
	source $SRC_BASE/config.sh
# ---------------------------------------------------------------------------------------------------------------
# Function definitions ------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------
# Reading options -----------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------
# Reading arguments ---------------------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------
# Main part -----------------------------------------------------------------------------------------------------
# Say HY
echo -e "$welcome"
echo
# Check that he know his password
sudo echo -e "$admin_true" || { echo -e "$admin_false"; exit 2; }
# Get correct tools and udate the system
echo "Testing System"
if [[ $OS == *"Linux"* ]]
then
	echo "In install linux"
	source $SRC_BASE/installer.sh
	fcy_install
else
	echo -e "$not_linux"
	echo -e "$not_supported_help"
	echo -e "$waiting_for_manual_installation"
	read -r
	source $SRC_BASE/check_version.sh
	fcy_check_installs
fi
# ---------------------------------------------------------------------------------------------------------------
fcy_config
