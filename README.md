# Welcome

This is a small tool for onboarding new dev in @famicity teams :). It will ensure that they are ready to start working.

## Welcome a new guy : 

- Install git. 
- Clone the repository `git clone git@github.com:titouanfreville/famicity_welcome.git`
