#!/bin/bash
# This contain only message variables in English
# Colors variable
# ### Weight Style ### #
bold="\e[1;"
thin="\e[0;"
# ### ### ### #
# ### COLORS ### #
green="39;32m"
red="39;31m"
grey="39;37m"
famicity_green="38;5;190m"
famicity_grey="38;5;246m"
basic="\\033[0;39m"
famicity="${bold}${famicity_green}fami$bold${famicity_grey}city"
# ### ### #
# Say hy message
welcome="${basic}Salut toi oh mon frère, et bienvenue chez $famicity$basic. 
Nous te souhaitons la bienvenue et espérons que tu passes un bon moment parmi nous. 
Pour t'aider à affronter au mieux cette nouvelle épreuves, nous allons te guider pas à pas dans la configuration de ton nouvel outils de travail :)."
# Hints
hints="
Raccourcie bash: 
- hello: A lancer le matin. Il mettra à jour l'environnement local, les fichiers gits et lancera le script ssh_setup
- commit: Lance git commit
- st: Lance git status
- dcu: Lance docker-compose up 
- dcs:  Lance docker-compose stop 
- dcb:  Lance docker-compose build
- git_update:  Lance un script de mise à jour des repos git de famicity.

Information pour les répertoires Git:
En mode terminal, si vous allez dans un répertoire git, vous pourrez obtenir différentes information de statut sur celui-ci et le nom de la branche courante. 

Si le répertoire est propre et à jour, vous verrez un Ok vert.
Après le nom de branche :
- Le chiffre précédé d'un plus indique le nombre de fichier suivis modifiés.
- Le chiffre suivant ... indique le nombre de fichier non suivis et non ignorés dans le répertoire. 
- La flèche montante indique le nombre de commit d'avance.
- La flèche montante indique le nombre de commit de retard.
- Le drapeau indique le nombre de stash appliqué à la branche. 
"
# General waiting
waiting_enter="Appuyer sur [Entrée pour continuer] ..."
# You are god message
admin_true="$thin${green}Vous êtes bien le dieu de cette machine.$basic"
admin_false="$bold${red}Vous n'êtes qu'un simple mortel. Retournez dans les limbes du Styx pour passer au rang supérieur.$basic"
# Upgrade message
upgrade_done="$thin${green}Votre système est à jour."
upgrade_fail="$thin${red}Votre système n'a pas pus être mis à jour."
# Install messages
install_ok="$bold${green}L'installation des packets <<$package_to_install>> c'est bien déroulée.$basic"
install_failed="$bold${red}L'installation des packets <<$package_to_install>> a échouée.$basic"
docker_ok="$bold${green}Docker a été correctement configuré. Pour pouvoir l'utiliser sans 'sudo', vous devez redémarrer votre session.$basic"
docker_failed="$bold${red}La configuration de Docker a échoée.$basic"
dc_ok="$bold${green}Docker-compose a été correctement configuré.$basic"
dc_failed="$bold${red}La configuration de Docker-compose a échouée.$basic"
not_supported_help="Pour pouvoir jouer avec nous, vous allez devoir installer : 
Docker, Docker-compose et Git. Vous allez également devoir créer une clé privée appelée my_private_key et sa clé publique. 

Vous pouvez consultez les pages suivantes pour vous aidez : 
- https://docs.docker.com/engine/installation/
- https://docs.docker.com/compose/install/
- https://desktop.github.com/
- https://help.github.com/articles/generating-an-ssh-key/
"
not_linux="$thin${red}Vous n'utilisez pas linux. Votre système n'est actuellement pas supportée$basic"
unsupported_distribution="$thin${red}Votre distribution Linux n'est pas encore supportée.$basic"
waiting_for_manual_installation="En attente de l'installation manuelle. $waiting_enter"
# Checker message
docker_not_installed="$bold${red}Please Install docker from https://docs.docker.com/engine/installation/ or make it run without sudo. Read the full doc :)"
dc_not_installed="$bold${red}Please Install docker compose from https://docs.docker.com/compose/install/ $basic"
git_installed="$thin${green}Git is correctly installed.$basic"
git_not_installed="$bold${red}Git is not installed, please install it https://desktop.github.com/ $basic"
version_older="$thin${red}Your version of $soft is not up to date. Please upgrade it to $soft_required. Current is $soft_existing$basic"
version_ok="$thin${green}$soft version is correctly supported. Please provide feed back on bugs.$basic"
# Config message
ssh_key_generated="$thin${green}La clé ssh a bien été générée.$basic"
ssh_key_not_generated="$bold${red}Erreur lors de la génération de la clé ssh.$basic"
add_key_to_git="Veillez passer la clé publique suivante dans git (elle devra déjà être dans le presse papier). 
Étapes : 
- Loggez vous dans git. 
- Allez dans configuration > Clés SSH et GPG.
- Ajoutez une clé ssh.
"
waiting_add_key_git="En attente de l'ajout de la clé dans git."
start_key_print=">>>> Début de la clé publique"
end_key_print=">>>> Fin de la clé publique"
git_ready="$bold${green}Git est bien configuré pour $famicity.$basic"
git_failed="$bold${red}Git n'a pas été correctement configuré.$basic"
getted_cluser="$thin${green}Le cluster pour $famicity $thin${green}a bien été synchroniser.$basic"
failed_getting_cluser="$bold${red}Erreur lors de l'obtention du cluster.$basic"
famicity_getted="$thin${green}Le code source du projet a bien été synchronisé.$basic"
failed_famicity_getted="$bold${red}Erreur lors de l'obtention du code source.$basic"
famicity_builded="$famicity $thin${green}est prêt à être amélioré :).$basic"
failed_famicity_build="$bold${red}Erreur lors de la construction des containers $famicity.$basic"
famicity_configured="Le projet $famicity$basic a bien été configuré. Voici quelques astuces :
$hints"
