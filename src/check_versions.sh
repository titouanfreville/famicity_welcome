#!/bin/bash
# Variables
min_docker_version=1.12
min_docker_compose_version=1.8
# Functions
version_comp () {
    if [[ $1 == $2 ]]
    then
        return 0
    fi
    local IFS=.
    local i ver1=($1) ver2=($2)
    # fill empty fields in ver1 with zeros
    for ((i=${#ver1[@]}; i<${#ver2[@]}; i++))
    do
        ver1[i]=0
    done
    for ((i=0; i<${#ver1[@]}; i++))
    do
        if [[ -z ${ver2[i]} ]]
        then
            # fill empty fields in ver2 with zeros
            ver2[i]=0
        fi
        if ((10#${ver1[i]} > 10#${ver2[i]}))
        then
            return 0
        fi
        if ((10#${ver1[i]} < 10#${ver2[i]}))
        then
            return 2
        fi
    done
    return 0
}

test_version_comp () {
    local soft_existing=$1
    local soft_required=$2
    local soft=$3
    version_comp $1 $2
    case $? in
        0) op='>=';;
        *) op='<';;
    esac
    if [[ $op = '<' ]]
    then
        echo -e "$version_older"
        return 1
    else
        echo -e "$version_ok"
        return 0
    fi
}

docker_ver () {
  echo -e "$test_docker_install"
  VERSION=$(sudo docker version --format '{{.Server.Version}}')
  if [ $? -eq 0 ]
  then
    test_version_comp $VERSION $min_docker_version "Docker" || exit 1;
  else
    echo -e "$docker_not_installed"
    exit 1;
  fi
  echo -e "$end_version_check"
  echo -e "$check_dc_install"
  VERSION=$(docker-compose version --short)
  if [ $? -eq 0 ]
  then
    echo
    test_version_comp $VERSION $min_docker_compose_version "Docker compose" || exit 2;
  else
    echo -e "$dc_not_installed"
    exit 2
  fi
  echo -e "$end_version_check"
}

fcy_check_installs () {
    docker_ver
    git --version > /dev/null 2> /dev/null && echo -e "$git_installed" || { echo -e "$git_not_installed"; exit 1; }
}