#!/bin/bash
# Bases Variables -----------------------------
package_to_install="docker-engine git vim figlet fortunes-fr cowsay xclip"
dc_version=1.8.1
# Functions -----------------------------------
fcy_install () {
	echo "$(cat /etc/*-release)" > tmp.version && sed -i "s/^/export /g" tmp.version && source tmp.version && rm -f tm.version
	case $DISTRIB_ID in
		Ubuntu) 
			sudo apt-key add --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
			sudo bash -c 'echo "$(cat /etc/*-release)" > tmp.version && sed -i "s/^/export /g" tmp.version && \
			source tmp.version && echo "deb https://apt.dockerproject.org/repo ubuntu-$DISTRIB_CODENAME main" > /etc/apt/sources.list.d/docker.list }'
			rm -f tmp.version
			;;
		*) 
			echo -e "$unsupported_distribution"
			echo -e "$not_supported_help"
			echo -e "$waiting_for_manual_installation"
			read -r
			;;
	esac
	{ sudo apt-get update && sudo apt-get upgrade -y && sudo apt-get dist-upgrade -y && sudo apt-get autoremove -y; } && echo -e "$upgrade_done" || { echo -e "$upgrade_fail" && exit 1; }
	sudo apt-get install -y $package_to_install && echo -e "$install_ok" || { echo -e "$install_failed"; exit 2; }
	{ sudo usermod -aG docker $USER && sudo docker run hello-world; }  && echo -e "$docker_ok" || { echo -e "$docker_failed" &&  exit 3; }
	{ sudo bash -c 'curl -L "https://github.com/docker/compose/releases/download/$dc_version/docker-compose-$(uname -s)-$(uname -m)" > /usr/local/bin/docker-compose' && \
	chmod +x /usr/local/bin/docker-compose && \
	sudo bash -c 'curl -L https://raw.githubusercontent.com/docker/compose/$(docker-compose version --short)/contrib/completion/bash/docker-compose > /etc/bash_completion.d/docker-compose' && \
	docker-compose --version; } && echo -e "$dc_ok" || { echo -e "$dc_failed" && exit 4; } 
}
