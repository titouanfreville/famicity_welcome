#!/bin/bash
fcy_config () {
	cp -f $SRC_BASE/.bashrc ~/.bashrc
	cd ~
	git clone https://github.com/magicmonty/bash-git-prompt.git .bash-git-prompt --depth=1
	cd -
	cp -f $SRC_BASE/.git_commit_message.txt ~/.git_commit_message.txt
	git config --global commit.template ~/.git_commit_message.txt
	cd tmp
	wget -q http://freville.iiens.net/scripts/installation.sh && chmod +x installation.sh && ./installation.sh && rm installation.sh 
	cd -
	echo -e "$fcy_starting_config"
	sudo bash -c 'echo "127.0.0.1 front-development.famicity.com
127.0.0.1 development.famicity.com
127.0.0.1 api-development.famicity.com
127.0.0.1 auth-development.famicity.com
127.0.0.1 crm-development.famicity.com
127.0.0.1 push-development.famicity.com
127.0.0.1 push-private-development.famicity.com
127.0.0.1 sidekiqmonitor-development.famicity.com
127.0.0.1 static1-development.famicity.com
127.0.0.1 static2-development.famicity.com
127.0.0.1 static3-development.famicity.com
127.0.0.1 store-development.famicity.com
127.0.0.1 localstore-development.famicity.com
127.0.0.1 mobilefront.famicity.com" >> /etc/hosts'
	ssh-keygen -t rsa -f $HOME/.ssh/my_private_key -q -N "" && \
	eval "$(ssh-agent -s)" > /dev/null 2> /dev/null && \
    ssh-add ~/.ssh/my_private_key && \
    xclip -sel clip < ~/.ssh/my_private_key.pub && \
    echo -e "$ssh_key_generated" || { echo -e "$ssh_key_not_generated";  }
    echo -e "$add_key_to_git"
    echo -e "$waiting_add_key_git"
    echo -e "$start_key_print"
    cat ~/.ssh/my_private_key.pub
    echo -e "$end_key_print" 
    echo -e "$waiting_enter"
    read -r
    ssh -T git@github.com > /dev/null 2> /dev/null
    if [ $? -eq 1 ] 
	then 
		echo -e "$git_ready"
	else 
		echo -e "$git_failed"
		exit 2
	fi
    cd ~
    git clone git@github.com:famicity/docker_cluster.git && echo -e "$getted_cluser" || { echo -e "$failed_getting_cluser"; exit 3; }
    cd docker_cluster
    { ./ssh_setup.sh && ./get_the_code.sh; } && echo -e "$famicity_getted" || { echo -e "$failed_famicity_getted"; exit 4; }
    sudo docker-compose build && echo -e "$famicity_builded" || { echo -e "$failed_famicity_build"; exit 5; }
    echo -e "$famicity_configured"
}