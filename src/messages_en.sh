#!/bin/bash
# This contain only message variables in English
# Colors variable
# ### Weight Style ### #
bold="\e[1;"
thin="\e[0;"
# ### ### ### #
# ### COLORS ### #
green="39;32m"
red="39;31m"
grey="39;37m"
famicity_green="38;5;190m"
famicity_grey="38;5;246m"
basic="\\033[0;39m"
# ### ### #
# Say hy message
welcome="${basic}Hello new guy and welcome in ${bold}${famicity_green}fami$bold${famicity_grey}city$basic team. 
We hope you will have a good time with us. 
To help you getting started, we are going to configure together your new working tools :)."
# General Wait
waiting_enter"Press [Enter] when you are done ..."
# You are god message
admin_true="$thin${green}You are god.$basic"
admin_false="$bold${red}You are not god, please see what is the problem.$basic"
# Upgrade message
upgrade_done="$thin${green}Your system is up to date.$basic"
upgrade_fail="$thin${red}We could not upgrade your system.$basic"
# Specific bashrc shortcut
hints="
Bash shortcuts: 
- hello: run it when you launch your computer on the morning. It will do basic step that are usefull at the start of a day (update your system, update git dependencies, run ssh-config)
- commit: run git commit command
- st: run git status command
- dcu: run docker-compose up 
- dcs: run docker-compose stop 
- dcb: run docker-compose build
- git_update: run a script to update all your git dependencies for famicity.

Git repository information:
When you go into a git repository path from terminal, you will have information on the status of this path. 
If you are cleaned and up to date, you will have a green check.
After the branch name, the cross show the number of followed files modified. 
After the ... it indicate the number of unfollowed files in the repository. 
The up arrow stand for commit advance.
The down arrow stand for commit behind. 
The flag show number of stash on your local copy of the banch. 
"
# Install messages
install_ok="$bold${green}Install of $package_to_install correctly done.$basic"
install_failed="$bold${red}Install of $package_to_install failed.$basic"
docker_ok="$bold${green}Docker is ready. Restart your session at end of process to use it without sudo.$basic"
docker_failed="$bold${red}Docker installation whent wrong.$basic"
dc_ok="$bold${green}Docker-compose is ready.$basic"
dc_failed="$bold${red}Docker-compose installation whent wrong.$basic"
not_supported_help="To be ready to play with us, you have to install the followings tools : 
Docker, Docker Compose and Git. Also, you are required to crete a set of private/public key named my_private_key(.pub)
You can consult those links to have help : 
- https://docs.docker.com/engine/installation/
- https://docs.docker.com/compose/install/
- https://desktop.github.com/
- https://help.github.com/articles/generating-an-ssh-key/
"
not_linux="$thin${red}You are not on a linux system. Install is not supported yet$basic"
unsupported_distribution="$thin${red}Your Linux distribution is not supported yet. Please proced manually$basic"
waiting_for_manual_installation="Waiting for your manual installation. $waiting_enter"
# Checker message
docker_not_installed="$bold${red}Please Install docker from https://docs.docker.com/engine/installation/ or make it run without sudo. Read the full doc :)"
dc_not_installed="$bold${red}Please Install docker compose from https://docs.docker.com/compose/install/ $basic"
git_installed="$thin${green}Git is correctly installed.$basic"
git_not_installed="$bold${red}Git is not installed, please install it https://desktop.github.com/ $basic"
version_older="$thin${red}Your version of $soft is not up to date. Please upgrade it to $soft_required. Current is $soft_existing$basic"
version_ok="$thin${green}$soft version is correctly supported. Please provide feed back on bugs.$basic"
# Config message
ssh_key_generated="$thin${green}Ssh key correctly generated.$basic"
ssh_key_not_generated="$bold${red}Failed to generate ssh key.$basic"
add_key_to_git="Please copy the public key part into git (it should be in your clipboard already, else it is print bellow). 
Steps : 
- Log into git. 
- Go in settings.
- Go into SSH anf GPG keys.
- Add a new ssh key.
"
waiting_add_key_git="Wainting for you to add key to git."
start_key_print=">>>> Begin of public key"
end_key_print=">>>> End of public key"
git_ready="$bold${green}Git is correctly configure for famicty usaged.$basic"
git_failed="$bold${red}Git is not correctly configure for famicty usaged.$basic"
getted_cluser="$thin${red}Correctly synced famicity's cluster.$basic"
failed_getting_cluser="$bold${green}Failed to retrive famicity's cluster from git.$basic"
famicity_getted="$thin${green}Well getted famicy's sources.$basic"
failed_famicity_getted="$bold${red}Coud not retrive sources from git.$basic"
famicity_builded="$thin${green}Famicity ready to use.$basic"
failed_famicity_build="$bold${red}Error while building famicity containers.$basic"
famicity_configured="You are ready to use famicity. Here are some hints :
$hints"